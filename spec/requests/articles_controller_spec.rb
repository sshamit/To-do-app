require 'rails_helper'

RSpec.describe 'Articles API', type: :request do
  # initialize test data
  let!(:articles) { create_list(:article, 10) }
  let(:article_id) { articles.first.id }

  # Test suite for GET /api/v1/articles
  describe 'GET /api/v1/articles' do
    # make HTTP get request before each example
    before do
      @admin_user = User.create!(email: "test@example.com", password: "12345678")
      # @request = FakeRequest.new(headers: { "X-USER-TOKEN" => token, "X-USER-EMAIL" => "test@example.com"  })
      token = Tiddle.create_and_return_token(@admin_user, FakeRequest.new)
      @admin_user.authentication_tokens.build(body: token)
      @admin_user.save!

      get '/api/v1/articles',headers: {"X-USER-TOKEN" => token, "X-USER-EMAIL" => "test@example.com" }
    end


    it 'returns articles' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json['data'].size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /api/v1/articles/:id
  describe 'GET /api/v1/articles/:id' do
    # before { get "/api/v1/articles/#{article_id}" }
    before do
      @admin_user = User.create!(email: "test@example.com", password: "12345678")
      # @request = FakeRequest.new(headers: { "X-USER-TOKEN" => token, "X-USER-EMAIL" => "test@example.com"  })
      token = Tiddle.create_and_return_token(@admin_user, FakeRequest.new)
      @admin_user.authentication_tokens.build(body: token)
      @admin_user.save!

      get "/api/v1/articles/#{article_id}" ,headers: {"X-USER-TOKEN" => token, "X-USER-EMAIL" => "test@example.com" }
    end
    context 'when the record exists' do
      it 'returns the article' do
        expect(json['data']).not_to be_empty
        expect(json['data']['id']).to eq(article_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:article_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Article/)
      end
    end
  end

  # Test suite for POST /api/v1/articles
  describe 'POST /api/v1/articles' do
    # valid payload
    let(:valid_attributes) { { title: 'Learn Elm', body: '1' } }

    context 'when the request is valid' do
      # before { post '/api/v1/articles', params: valid_attributes }
      before do
        @admin_user = User.create!(email: "test@example.com", password: "12345678")
        # @request = FakeRequest.new(headers: { "X-USER-TOKEN" => token, "X-USER-EMAIL" => "test@example.com"  })
        token = Tiddle.create_and_return_token(@admin_user, FakeRequest.new)
        @admin_user.authentication_tokens.build(body: token)
        @admin_user.save!

        post '/api/v1/articles', params: valid_attributes ,headers: {"X-USER-TOKEN" => token, "X-USER-EMAIL" => "test@example.com" }
      end
      it 'creates a article' do
        expect(json['data']['title']).to eq('Learn Elm')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      # before { post '/api/v1/articles', params: { title: 'Foobar' } }
      before do
        @admin_user = User.create!(email: "test@example.com", password: "12345678")
        # @request = FakeRequest.new(headers: { "X-USER-TOKEN" => token, "X-USER-EMAIL" => "test@example.com"  })
        token = Tiddle.create_and_return_token(@admin_user, FakeRequest.new)
        @admin_user.authentication_tokens.build(body: token)
        @admin_user.save!

        post '/api/v1/articles', params: { title: 'Foobar' } ,headers: {"X-USER-TOKEN" => token, "X-USER-EMAIL" => "test@example.com" }
      end
      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(json['data'])
          .to match({"body"=>["can't be blank"]})
      end
    end
  end

  # Test suite for PUT /api/v1/articles/:id
  describe 'PUT /api/v1/articles/:id' do
    let(:valid_attributes) { { title: 'Shopping' } }

    context 'when the record exists' do
      # before { put "/api/v1/articles/#{article_id}", params: valid_attributes }
      before do
        @admin_user = User.create!(email: "test@example.com", password: "12345678")
        # @request = FakeRequest.new(headers: { "X-USER-TOKEN" => token, "X-USER-EMAIL" => "test@example.com"  })
        token = Tiddle.create_and_return_token(@admin_user, FakeRequest.new)
        @admin_user.authentication_tokens.build(body: token)
        @admin_user.save!

        put "/api/v1/articles/#{article_id}", params: valid_attributes ,headers: {"X-USER-TOKEN" => token, "X-USER-EMAIL" => "test@example.com" }
      end

      it 'updates the record' do
        expect(response.body).not_to be_empty
      end
    end
  end

  # Test suite for DELETE /api/v1/articles/:id
  describe 'DELETE /api/v1/articles/:id' do
    # before { delete "/api/v1/articles/#{article_id}" }
    before do
      @admin_user = User.create!(email: "test@example.com", password: "12345678")
      # @request = FakeRequest.new(headers: { "X-USER-TOKEN" => token, "X-USER-EMAIL" => "test@example.com"  })
      token = Tiddle.create_and_return_token(@admin_user, FakeRequest.new)
      @admin_user.authentication_tokens.build(body: token)
      @admin_user.save!

      delete "/api/v1/articles/#{article_id}" ,headers: {"X-USER-TOKEN" => token, "X-USER-EMAIL" => "test@example.com" }
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end
end
