module Api
  module V1
    class ApplicationController < ActionController::Base
      before_action :check_user!
      private
      def check_user!
        @user = User.find_by_email(request.headers["X-USER-EMAIL"])
        if @user
          token = Tiddle::TokenIssuer.build.find_token(@user, request.headers["X-USER-TOKEN"])
          return true unless token.respond_to?(:expires_in)
          return true if token.expires_in.blank? || token.expires_in.zero?
        else
          render json: {message: "User doesn't exist"}
        end
      end
    end
  end
end