module Api
  module V1
    class ArticlesController < Api::V1::ApplicationController
      
      include Response
      include ExceptionHandler
      
      def index
        articles = Article.order('created_at DESC');
        render json: {status:'SUCCESS',messege:'Loaded Articles',data:articles},status: :ok
      end

      def show
        article = Article.find(params[:id])
        render json: {status:'SUCCESS',messege:'Loaded Article',data:article},status: :ok
      end

      def create
        article = Article.new(article_params)
        if article.save
          render json: {status:'SUCCESS',messege:'Data Saved Successfully.',data:article},status: :created
        else
          render json: {status:'ERROR',messege:'Data did not save.',data:article.errors},status: :unprocessable_entity
        end
      end

      def destroy
        article = Article.find(params[:id])
        article.destroy
        render json: {status:'SUCCESS',messege:'Data Deleted Sucussfully',data:article},status: :ok
      end

      def update
        article = Article.find(params[:id])
        if article.update(article_params)
          render json: {status:'SUCCESS',messege:'Data Updated Successfully.',data:article},status: :ok
        else
          render json: {status:'ERROR',messege:'Data did not Updated.',data:article.errors},status: :unprocessable_entity
        end
      end

      private
      def article_params
        params.permit(:title, :body)
      end

    end
  end
end
