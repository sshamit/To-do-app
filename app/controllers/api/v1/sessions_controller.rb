module Api
  module V1
    class SessionsController < Api::V1::ApplicationController
      skip_before_action :check_user!

      def create
        @user = User.find_by_email(params[:email])
        if @user && @user.valid_password?(params[:password])
          token = Tiddle.create_and_return_token(@user, request)
          render json: { authentication_token: token }
        else
          render json: {message: "User doesn't exist"}
        end
      end


      def destroy
        Tiddle.expire_token(current_user, request) if current_user
        render json: {}
      end


    end
  end
end