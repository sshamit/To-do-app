Rails.application.routes.draw do

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users
  resources :todo_lists do
  	resources :todo_items do
  		member do
  			patch :complete
  		end
  	end
  end

  root "todo_lists#index"

  namespace 'api' do
    namespace 'v1' do
      resources :sessions, only: [:create, :destroy]
      resources :articles
    end
  end
end
